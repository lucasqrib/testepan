package com.lucasribeiro.pantest.components.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.lucasribeiro.pantest.R
import com.lucasribeiro.pantest.models.Game
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation

/**
 *
 * Created by Lucas on 30/10/2017.
 */
class GamesAdapter(private var gamesList: List<Game>, var context: Context) : RecyclerView.Adapter<GamesAdapter.VH>() {
    override fun onBindViewHolder(holder: VH, position: Int) {
        val topGame = gamesList[position]
        Picasso.with(context)
                .load(topGame.cover)
                .noFade()
                .error(R.drawable.image_error_layer_list)
                .placeholder(R.drawable.image_background)
                .transform(RoundedCornersTransformation(5, 5))
                .into(holder.gameCover)
        holder.gameTitle.text = topGame.localizedName
        holder.gameViewews.text = context.getString(R.string.viewers, topGame.viewers.toString())
        holder.gameChannels.text = context.getString(R.string.canais, topGame.channels.toString())
    }

    override fun getItemCount(): Int {
        return gamesList.size
    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): VH {
        return VH(LayoutInflater.from(parent?.context).inflate(R.layout.adapter_cell_game, parent, false))
    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var gameCover: ImageView = itemView.findViewById(R.id.game_cover)
        var gameTitle: TextView = itemView.findViewById(R.id.game_title)
        var gameViewews: TextView = itemView.findViewById(R.id.game_viewers)
        var gameChannels: TextView = itemView.findViewById(R.id.game_channels)
    }

}