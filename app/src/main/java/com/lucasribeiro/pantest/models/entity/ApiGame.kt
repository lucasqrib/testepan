package com.lucasribeiro.pantest.models.entity

import com.google.gson.annotations.SerializedName


/**
 *
 * Created by Lucas on 30/10/2017.
 */

data class ApiGame(
        @SerializedName("name") val name: String,
        @SerializedName("popularity") val popularity: Int,
        @SerializedName("_id") val id: Int,
        @SerializedName("giantbomb_id") val giantbombId: Int,
        @SerializedName("box") val box: ApiImageContainer,
        @SerializedName("logo") val logo: ApiImageContainer,
        @SerializedName("localized_name") val localizedName: String,
        @SerializedName("locale") val locale: String)


