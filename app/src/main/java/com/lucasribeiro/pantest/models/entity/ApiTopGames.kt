package com.lucasribeiro.pantest.models.entity

import com.google.gson.annotations.SerializedName

/**
 *
 * Created by Lucas on 30/10/2017.
 */

data class ApiTopGames(@SerializedName("_total") val total: Int,
                    @SerializedName("top") val top: List<ApiTop>? = null)

