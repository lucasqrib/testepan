package com.lucasribeiro.pantest.models.entity

import com.google.gson.annotations.SerializedName


/**
 *
 * Created by Lucas on 30/10/2017.
 */
data class ApiTop(
        @SerializedName("game") val game: ApiGame,
        @SerializedName("viewers") val viewers: Int,
        @SerializedName("channels") val channels: Int)
