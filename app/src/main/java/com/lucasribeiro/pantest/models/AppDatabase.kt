package com.lucasribeiro.pantest.models

import android.arch.persistence.room.*
import io.reactivex.Single

/**
 * Created by Lucas on 01/11/2017.
 *
 */
@Database(entities = arrayOf(Game::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun gamesDao(): GamesDao
}

@Dao
interface GamesDao {

    @Query("SELECT * FROM top_games")
    fun getTopGames(): Single<List<Game>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<Game>)
}