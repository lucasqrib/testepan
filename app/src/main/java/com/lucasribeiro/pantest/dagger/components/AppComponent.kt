package com.lucasribeiro.pantest.dagger.components

import com.lucasribeiro.pantest.Api
import com.lucasribeiro.pantest.BaseApp
import com.lucasribeiro.pantest.dagger.modules.common.ApiModule
import com.lucasribeiro.pantest.dagger.modules.common.AppModule
import com.lucasribeiro.pantest.dagger.modules.common.InteractorModule
import com.lucasribeiro.pantest.interactor.GamesInteractor
import com.lucasribeiro.pantest.models.AppDatabase
import dagger.Component
import javax.inject.Singleton

/**
 *
 * Created by Lucas on 30/10/2017.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class, InteractorModule::class, ApiModule::class))
interface AppComponent {
    fun inject(app: BaseApp)
    fun gamesInteractor(): GamesInteractor
    fun api(): Api
    fun appDatabase (): AppDatabase
}