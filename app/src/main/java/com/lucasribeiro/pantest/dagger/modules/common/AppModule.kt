package com.lucasribeiro.pantest.dagger.modules.common

import android.arch.persistence.room.Room
import com.lucasribeiro.pantest.BaseApp
import com.lucasribeiro.pantest.models.AppDatabase
import com.lucasribeiro.pantest.models.GamesDao
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val app: BaseApp) {

    @Provides
    internal fun provideApp(): BaseApp {
        return app
    }

    @Provides
    internal fun provideRoomDatabase(): AppDatabase {
        return Room.databaseBuilder(app,
                AppDatabase::class.java, "games-database").build()
    }

    @Provides
    internal fun provideGamesDao(roomDatabase: AppDatabase): GamesDao {
        return roomDatabase.gamesDao()
    }

}
