package com.lucasribeiro.pantest.presenters

/**
 *
 * Created by Lucas on 30/10/2017.
 */

interface GamesActivityPresenter {
    fun onInit()
    fun loadMoreGames(limit: Int, offset: Int)
    fun refreshGames(currentIndex: Int)
}