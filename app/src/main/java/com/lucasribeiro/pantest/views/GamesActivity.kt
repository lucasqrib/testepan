package com.lucasribeiro.pantest.views

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.lucasribeiro.pantest.BaseApp
import com.lucasribeiro.pantest.R
import com.lucasribeiro.pantest.components.adapters.GamesAdapter
import com.lucasribeiro.pantest.dagger.components.AppComponent
import com.lucasribeiro.pantest.dagger.components.DaggerGamesActivityComponent
import com.lucasribeiro.pantest.dagger.modules.GamesActivityModule
import com.lucasribeiro.pantest.models.Game
import com.lucasribeiro.pantest.presenters.GamesActivityPresenter
import kotlinx.android.synthetic.main.activity_games.*
import java.util.*
import javax.inject.Inject

class GamesActivity : AppCompatActivity(), GamesActivityView {
    override fun getContext(): Context {
        return this
    }

    private val gamesList: MutableList<Game> = ArrayList()

    private lateinit var gamesAdapter: GamesAdapter

    override fun showGames(top: List<Game>) {
        gamesList.addAll(top)
        gamesAdapter.notifyDataSetChanged()
    }

    @Inject
    lateinit var presenter: GamesActivityPresenter

    override fun setupComponent(appComponent: AppComponent) {
        DaggerGamesActivityComponent.builder()
                .appComponent(appComponent)
                .gamesActivityModule(GamesActivityModule(this))
                .build()
                .inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_games)
        setupComponent(BaseApp.appComponent)
        presenter.onInit()
        initRecyclerView()
        initSwipeToRefresh()

    }

    private fun initSwipeToRefresh() {
        main_swipe_layout.setOnRefreshListener({
            presenter.refreshGames(10)
        })
    }

    override fun replaceGames(top: List<Game>, isFreshData: Boolean) {
        gamesList.clear()
        gamesList.addAll(top)
        gamesAdapter.notifyDataSetChanged()
    }

    override fun stopRefresh() {
        main_swipe_layout.isRefreshing = false
    }


    private fun initRecyclerView() {
        gamesAdapter = GamesAdapter(gamesList, this)
        val gridLayoutManager = GridLayoutManager(this, 2)
        main_games_list.layoutManager = gridLayoutManager
        main_games_list.setHasFixedSize(true)
        main_games_list.addOnScrollListener(object : EndlessListener(gridLayoutManager, 10) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.loadMoreGames(10, page)
            }
        })
        main_games_list.adapter = gamesAdapter
    }

    override fun showProgress() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun stopProgress() {
        progress_bar.visibility = View.GONE
    }

    override fun showErrorScreen() {
        val snackbarFalha = Snackbar.make(root_container, getString(R.string.erro_ao_buscar), Snackbar.LENGTH_SHORT)
        val snackbarRefresh = Snackbar.make(root_container, getString(R.string.puxe_refresh), Snackbar.LENGTH_INDEFINITE)
        snackbarRefresh.setAction(getString(R.string.tente_novamente), {
            snackbarRefresh.dismiss()
            presenter.refreshGames(10)
        })
        snackbarFalha.addCallback(object : Snackbar.Callback() {
            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                super.onDismissed(transientBottomBar, event)
                snackbarRefresh.show()
            }
        })
        snackbarFalha.show()


    }

}
