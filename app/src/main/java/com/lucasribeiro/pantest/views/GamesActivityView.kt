package com.lucasribeiro.pantest.views

import android.content.Context
import com.lucasribeiro.pantest.dagger.components.AppComponent
import com.lucasribeiro.pantest.models.Game

/**
 *
 * Created by Lucas on 30/10/2017.
 */
interface GamesActivityView {
    fun setupComponent(appComponent: AppComponent)
    fun showGames(top: List<Game>)
    fun replaceGames(top: List<Game>, isFreshData: Boolean)
    fun stopRefresh()
    fun showProgress()
    fun showErrorScreen()
    fun stopProgress()
    fun getContext(): Context
}