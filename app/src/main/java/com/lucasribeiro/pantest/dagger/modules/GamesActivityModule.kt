package com.lucasribeiro.pantest.dagger.modules

import com.lucasribeiro.pantest.interactor.GamesInteractor
import com.lucasribeiro.pantest.presenters.GamesActivityPresenter
import com.lucasribeiro.pantest.presenters.GamesActivityPresenterImpl
import com.lucasribeiro.pantest.views.GamesActivityView
import dagger.Module
import dagger.Provides

/**
 *
 * Created by Lucas on 30/10/2017.
 */
@Module
class GamesActivityModule(val view: GamesActivityView) {


    @Provides
    fun provideGamesActivityView(): GamesActivityView {
        return this.view
    }

    @Provides
    fun providesGamesActivityPresenter(view: GamesActivityView, interactor: GamesInteractor): GamesActivityPresenter {
        return GamesActivityPresenterImpl(view, interactor)
    }
}