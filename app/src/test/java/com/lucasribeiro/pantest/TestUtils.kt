package com.lucasribeiro.pantest

import com.lucasribeiro.pantest.models.entity.ApiGame
import com.lucasribeiro.pantest.models.entity.ApiImageContainer
import com.lucasribeiro.pantest.models.entity.ApiTop
import com.lucasribeiro.pantest.models.entity.ApiTopGames

/**
 *
 * Created by Lucas on 02/11/2017.
 */

class TestUtils {
    companion object {
        fun getTopGamesMock(quantity: Int): ApiTopGames {
            val list = ArrayList<ApiTop>()
            repeat(quantity, {
                list.add(it, getTopMock(it))
            })
            return ApiTopGames(
                    total = quantity,
                    top = list
            )
        }

        private fun getTopMock(index: Int): ApiTop {

            return ApiTop(
                    viewers = 100 * index,
                    channels = 10 * index,
                    game = getGameMock(index)
            )
        }

        private fun getGameMock(id: Int): ApiGame {
            return ApiGame(
                    localizedName = "Test Game " + id,
                    id = id,
                    name = "Test Game",
                    giantbombId = 123456 + id,
                    popularity = 125 + id,
                    locale = "PT-BR",
                    box = getImageContainerMock(),
                    logo = getImageContainerMock()
            )
        }

        private fun getImageContainerMock(): ApiImageContainer {
            return ApiImageContainer(
                    large = "http://via.placeholder.com/150x300",
                    medium = "http://via.placeholder.com/150x300",
                    small = "http://via.placeholder.com/150x300",
                    template = "http://via.placeholder.com/150x300"
            )
        }
    }


}