package com.lucasribeiro.pantest

import com.lucasribeiro.pantest.models.entity.ApiTopGames
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 *
 * Created by Lucas on 30/10/2017.
 */
interface Api {
    @GET("games/top")
    fun getTopGames(@Query("limit") limit: Int, @Query("offset") offset: Int): Observable<ApiTopGames>

}