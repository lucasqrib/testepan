package com.lucasribeiro.pantest.models

import com.lucasribeiro.pantest.models.entity.ApiTop
import com.lucasribeiro.pantest.models.entity.ApiTopGames

/**
 *
 * Created by Lucas on 01/11/2017.
 */

class GameMapper {
    companion object {
        fun map(apiTopGames: ApiTopGames): List<Game> {
            val gamesList = ArrayList<Game>()
            apiTopGames.top?.map { topGames: ApiTop ->
                val game = Game(
                        id = topGames.game.id,
                        cover = topGames.game.box.large,
                        channels = topGames.channels,
                        localizedName = topGames.game.localizedName,
                        viewers = topGames.viewers)
                gamesList.add(game)
            }
            return gamesList
        }
    }
}