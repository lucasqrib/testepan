package com.lucasribeiro.pantest

import android.app.Application
import com.lucasribeiro.pantest.dagger.components.AppComponent
import com.lucasribeiro.pantest.dagger.components.DaggerAppComponent
import com.lucasribeiro.pantest.dagger.modules.common.AppModule

/**
 *
 * Created by Lucas on 30/10/2017.
 */

class BaseApp : Application() {
    companion object {
        lateinit var appComponent: AppComponent
    }


    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }


}
