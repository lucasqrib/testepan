package com.lucasribeiro.pantest.views

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView


/**
 *
 * Created by Lucas on 30/10/2017.
 */

abstract class EndlessListener(val mLayoutManager: GridLayoutManager, val loadQuantity: Int) : RecyclerView.OnScrollListener() {

    private var isLoading = true
    private var startingPageIndex = 0
    private var oldQuantity = 0

    override fun onScrolled(view: RecyclerView, dx: Int, dy: Int) {

        val totalItemCount = mLayoutManager.itemCount
        startingPageIndex = totalItemCount
        if (mLayoutManager.findLastCompletelyVisibleItemPosition() >= totalItemCount - (loadQuantity * 0.6) &&
                totalItemCount != oldQuantity) {
            isLoading = true
            oldQuantity = totalItemCount
            onLoadMore(startingPageIndex, totalItemCount, view)
        } else {
            isLoading = false
        }
    }


    abstract fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?)

}