package com.lucasribeiro.pantest.interactor

import android.util.Log
import com.lucasribeiro.pantest.Api
import com.lucasribeiro.pantest.models.Game
import com.lucasribeiro.pantest.models.GameMapper
import com.lucasribeiro.pantest.models.GamesDao
import com.lucasribeiro.pantest.models.entity.ApiTopGames
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * Created by Lucas on 30/10/2017.
 *
 */

class GamesInteractorImpl(val api: Api, val gamesDao: GamesDao) : GamesInteractor {
    override fun getGamesFromAPI(limit: Int, offset: Int): Observable<List<Game>> {
        return api.getTopGames(limit, offset)
                .filter { it.top?.isNotEmpty()!! }
                .map { topGames: ApiTopGames ->
                    GameMapper.map(topGames)
                }
    }

    override fun getGamesFromDB(): Observable<List<Game>> {
        return gamesDao.getTopGames()
                .toObservable()
    }

    override fun saveGames(games: List<Game>) {
        Observable.fromCallable { gamesDao.insertAll(games) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Log.i(this::class.java.simpleName, "Games inseridos")
                }
    }
}