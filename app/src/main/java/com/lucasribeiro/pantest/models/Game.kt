package com.lucasribeiro.pantest.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


/**
 *
 * Created by Lucas on 30/10/2017.
 */

@Entity(tableName = "top_games")
data class Game(
        @PrimaryKey @ColumnInfo(name = "id") val id: Int,
        @ColumnInfo(name = "cover") val cover: String,
        @ColumnInfo(name = "viewers") val viewers: Int,
        @ColumnInfo(name = "channels") val channels: Int,
        @ColumnInfo(name = "localized_name") val localizedName: String)


