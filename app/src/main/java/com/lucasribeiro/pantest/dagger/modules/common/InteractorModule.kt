package com.lucasribeiro.pantest.dagger.modules.common

import com.lucasribeiro.pantest.Api
import com.lucasribeiro.pantest.interactor.GamesInteractor
import com.lucasribeiro.pantest.interactor.GamesInteractorImpl
import com.lucasribeiro.pantest.models.GamesDao
import dagger.Module
import dagger.Provides


@Module
class InteractorModule {

    @Provides
    fun provideGamesRepository(api: Api, gamesDao: GamesDao): GamesInteractor {
        return GamesInteractorImpl(api, gamesDao)
    }

}
