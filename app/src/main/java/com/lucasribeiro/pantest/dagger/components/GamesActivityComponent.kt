package com.lucasribeiro.pantest.dagger.components

import com.lucasribeiro.pantest.dagger.modules.GamesActivityModule
import com.lucasribeiro.pantest.dagger.scopes.ActivityScope
import com.lucasribeiro.pantest.views.GamesActivity
import dagger.Component

/**
 *
 * Created by Lucas on 30/10/2017.
 */
@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(GamesActivityModule::class))
interface GamesActivityComponent {
    fun inject(activity: GamesActivity)
}