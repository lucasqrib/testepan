package com.lucasribeiro.pantest.models.entity

import com.google.gson.annotations.SerializedName


/**
 *
 * Created by Lucas on 30/10/2017.
 */
data class ApiImageContainer(
        @SerializedName("large") val large: String,
        @SerializedName("medium") val medium: String,
        @SerializedName("small") val small: String,
        @SerializedName("template") val template: String)
