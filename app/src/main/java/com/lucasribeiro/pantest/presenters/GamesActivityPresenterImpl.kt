package com.lucasribeiro.pantest.presenters

import com.lucasribeiro.pantest.interactor.GamesInteractor
import com.lucasribeiro.pantest.models.Game
import com.lucasribeiro.pantest.views.GamesActivityView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 *
 * Created by Lucas on 30/10/2017.
 */

class GamesActivityPresenterImpl(val view: GamesActivityView, val interactor: GamesInteractor) : GamesActivityPresenter {
    override fun refreshGames(currentIndex: Int) {
        interactor.getGamesFromAPI(currentIndex, 0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(
                        {
                            view.stopRefresh()
                            view.replaceGames(it, true)
                            interactor.saveGames(it)
                        },
                        {
                            it.printStackTrace()
                            view.stopRefresh()
                            view.showErrorScreen()
                        })
    }


    override fun onInit() {
        loadMoreGames(10, 0)
    }


    override fun loadMoreGames(limit: Int, offset: Int) {
        view.showProgress()
        interactor.getGamesFromAPI(limit, offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            view.stopProgress()
                            view.showGames(it)
                            interactor.saveGames(it)
                        },
                        {
                            getGameFromDB()
                        })
    }

    private fun getGameFromDB() {
        interactor.getGamesFromDB()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            if (it.isNotEmpty()) {
                                val sortedList = it.sortedWith(compareBy(Game::viewers)).asReversed()
                                view.replaceGames(sortedList, false)
                            }
                            view.stopProgress()
                            view.showErrorScreen()

                        },
                        {
                            it.printStackTrace()
                            view.stopProgress()
                            view.showErrorScreen()
                        }
                )
    }

}