package com.lucasribeiro.pantest.interactor

import com.lucasribeiro.pantest.models.Game
import io.reactivex.Observable

/**
 * Created by Lucas on 30/10/2017.
 *
 */

interface GamesInteractor {
    fun getGamesFromAPI(limit: Int, offset: Int): Observable<List<Game>>
    fun getGamesFromDB(): Observable<List<Game>>

    fun saveGames(games: List<Game>)
}