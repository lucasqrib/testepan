import com.lucasribeiro.pantest.Api
import com.lucasribeiro.pantest.TestUtils
import com.lucasribeiro.pantest.interactor.GamesInteractor
import com.lucasribeiro.pantest.interactor.GamesInteractorImpl
import com.lucasribeiro.pantest.models.Game
import com.lucasribeiro.pantest.models.GameMapper
import com.lucasribeiro.pantest.models.GamesDao
import com.lucasribeiro.pantest.presenters.GamesActivityPresenter
import com.lucasribeiro.pantest.presenters.GamesActivityPresenterImpl
import com.lucasribeiro.pantest.views.GamesActivityView
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.observers.TestObserver
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.Spy
import org.mockito.junit.MockitoJUnit


class GamesInteractorTest {

    @Rule
    @JvmField
    val rule = MockitoJUnit.rule()!!

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    @Mock
    lateinit var client: Api

    @Mock
    lateinit var gamesDao: GamesDao

    @Mock
    lateinit var viewMock: GamesActivityView

    @Spy
    lateinit var interactorMock: GamesInteractor

    private lateinit var interactorToTest: GamesInteractor
    private lateinit var presenterToTest: GamesActivityPresenter
    private val gamesResponse = TestUtils.getTopGamesMock(10)
    @Before
    fun setUp() {
        interactorToTest = GamesInteractorImpl(client, gamesDao)
        presenterToTest = GamesActivityPresenterImpl(viewMock, interactorMock)
    }

    @Test
    fun `Presenter init calling loadMoreMethod`() {
        Mockito.`when`(interactorMock.getGamesFromAPI(10, 0)).thenReturn(Observable.just(GameMapper.map(gamesResponse)))
        val sPresenter = spy(presenterToTest)
        sPresenter.onInit()
        verify(sPresenter, times(1)).loadMoreGames(10, 0)
    }

    @Test
    fun `Presenter handling success in API response`() {
        Mockito.`when`(interactorMock.getGamesFromAPI(10, 0)).thenReturn(Observable.just(GameMapper.map(gamesResponse)))
        presenterToTest.loadMoreGames(10, 0)
        val inOrder = Mockito.inOrder(viewMock)
        inOrder.verify(viewMock, times(1)).showProgress()
        inOrder.verify(viewMock, times(1)).stopProgress()
        inOrder.verify(viewMock, never()).showErrorScreen()
        inOrder.verify(viewMock, times(1)).showGames(GameMapper.map(gamesResponse))
    }

    @Test
    fun `Presenter handling failure in API response`() {
        val exception = Exception()
        Mockito.`when`(interactorMock.getGamesFromAPI(10, 0)).thenReturn(Observable.error<List<Game>>(exception))
        Mockito.`when`(interactorMock.getGamesFromDB()).thenReturn(Observable.just(GameMapper.map(gamesResponse)))
        presenterToTest.loadMoreGames(10, 0)
        val inOrder = Mockito.inOrder(viewMock)
        inOrder.verify(viewMock, times(1)).showProgress()
        inOrder.verify(viewMock, times(1)).replaceGames(GameMapper.map(gamesResponse).reversed(), false)
        inOrder.verify(viewMock, times(1)).stopProgress()
        inOrder.verify(viewMock, times(1)).showErrorScreen()

    }

    @Test
    fun `Game APIObject mapped to fit View Object`() {

        Mockito.`when`(client.getTopGames(10, 0)).thenReturn(Observable.just(gamesResponse))
        val result = interactorToTest.getGamesFromAPI(10, 0)

        val testObserver = TestObserver<List<Game>>()
        result.subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        val listResult = testObserver.values()
        assertThat(listResult[0].size, `is`(10))
        repeat(10, {
            assertThat(listResult[0][it].id, `is`(it))
            assertThat(listResult[0][it].localizedName, `is`("Test Game " + it))
            assertThat(listResult[0][it].cover, `is`("http://via.placeholder.com/150x300"))
            assertThat(listResult[0][it].channels, `is`(it * 10))
            assertThat(listResult[0][it].viewers, `is`(it * 100))
        })

    }
}

class RxImmediateSchedulerRule : TestRule {
    override fun apply(base: Statement, d: Description): Statement {
        return object : Statement() {
            @Throws(Throwable::class)
            override fun evaluate() {
                RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
                RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
                RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
                RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }

                try {
                    base.evaluate()
                } finally {
                    RxJavaPlugins.reset()
                    RxAndroidPlugins.reset()
                }
            }
        }
    }
}

